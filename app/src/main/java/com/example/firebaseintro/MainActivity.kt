package com.example.firebaseintro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TAG = "FirebaseDebug"

        val db = FirebaseFirestore.getInstance()

        db.collection("books")
            .addSnapshotListener{ value, error ->
                value.let{
                    for (document in value?.documents!!)
                    {
                        Log.d(TAG, "Name: ${document?.getString("name")}")
                        Log.d(TAG, "Description: ${document?.getString("description")}")
                    }
                }
            }

        /*
        db.collection("books")
            .document("001")
            .addSnapshotListener { value, error ->
                value.let{ document ->
                    document.let{
                        Log.d(TAG, "Name: ${document?.getString("name")}")
                        Log.d(TAG, "Description: ${document?.getString("description")}")
                    }
                }
            }

         */

        /*
        db.collection("books")
            .document()
            .set(Book("Programming in GO", "Learn Go"))
            .addOnSuccessListener {
                Log.d(TAG, "Book saved")
            }
            .addOnFailureListener{ exception ->
                Log.d(TAG, "An error succed")
            }

         */

        /*
        db.collection("books")
            .whereEqualTo("name", "Programming in C++")
            .get()
            .addOnSuccessListener { result ->
                for (document in result){
                    Log.d(TAG, "Name: ${document.getString("name")}")
                    Log.d(TAG, "Description: ${document.getString("description")}")
                }
            }

         */

        /*
        db.collection("books")
            .document("001")
            .get()
            .addOnSuccessListener { document ->
                document.let{
                    Log.d(TAG, "Name: ${document.getString("name")}")
                    Log.d(TAG, "Description: ${document.getString("description")}")
                }
            }

         */

        /*Log.d(TAG, "Get all documents")

        db.collection("books")
            .get()
            .addOnSuccessListener { result ->

                for (document in result){
                    Log.d(TAG, "Name: ${document.getString("name")}")
                    Log.d(TAG, "Description: ${document.getString("description")}")
                }

            }
            .addOnFailureListener{

            }

         */
    }

    data class Book(var name: String = "", var description:String = "")
}